package com.wantwant;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @program: sqplserver_springboot
 * @description: 应用启动入口
 * @author: Sunhaoyue
 * @create: 2019/06/17 15:04
 */

@SpringBootApplication
public class StartApplication {
    public static void main(String[] args)  {
        SpringApplication.run(StartApplication.class, args);
    }
}
